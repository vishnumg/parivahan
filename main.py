from parivahan import Parivahan
import pandas
import csv

if __name__ == '__main__':

    inDf = pandas.read_excel('in.xls', index_col=None)

    autoCaptcha = input("Automate captcha ? [Y / N] : ").lower() == "y"
    pv = Parivahan('fb5be0ba85dc5d60dbb1783231931471', autoCaptcha = autoCaptcha)
    try :
        preDf = pandas.read_csv("out.csv")
        preLen = len(preDf)
    except Exception as e :
        preLen = 0

    for i, row in inDf.iterrows():
        vNo = row["Registration No."]
        data = pv.getParivahanData(vNo)
        # outData.append(data)
        df = pandas.DataFrame([data])

        if i == 0 and preLen == 0: 
            df.to_csv('out.csv', mode='a', index = False)
        else :
            df.to_csv('out.csv', mode='a', index = False,  header = None)
    pv.close()