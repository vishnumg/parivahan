from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
from captcha_solver import CaptchaSolver


class Parivahan() :
    URL = r"https://vahan.nic.in/nrservices/faces/user/searchstatus.xhtml" 

    xPaths = [
        ("Registration No.", '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[3]/div[2]', 'Error' ),
        ("Owner's Name", '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[5]/div[2]' , 'Error'),
        ("Registration Date", '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[3]/div[4]' , 'Error'),
        ("Chassis No" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[4]/div[2]', 'Error'),
        ("Vehicle class" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[6]/div[2]', 'Error'),
        ("Maker / Model" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[7]/div[2]', 'Error'),
        ("Fitness/REGN Upto " , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[8]/div[2]', 'Error'),
        ("Insurance Upto" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[9]/div[2]', 'Error'),
        ("Emission norms" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[10]/div[2]', 'Error'),
        ("Engine No" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[4]/div[4]', 'Error'),
        ("Fuel" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[6]/div[4]', 'Error'),
        ("MV Tax upto" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[8]/div[4]', 'Error'),
        ("PUCC Upto" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[9]/div[4]', 'Error'),
        ("RC Status" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[10]/div[4]', 'Error'),
        ("Financed" , '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[5]/div/div/div/div[11]/div[2]', 'NO')
    ]
    
    def __init__( self, api_key, autoCaptcha = False ) :
        self.driver = webdriver.Chrome()
        self.autoCaptcha = autoCaptcha 
        self.driver.get(self.URL)
        if autoCaptcha:
            self.solver = CaptchaSolver('2captcha', api_key=api_key)


    def getParivahanData( self, vNo ):
        vNoBox = self.driver.find_elements(By.XPATH, '//*[@id="regn_no1_exact"]')[0]
        vNoBox.send_keys(Keys.CONTROL + "a");
        vNoBox.send_keys(Keys.DELETE);

        vNoBox.send_keys(vNo)

        captchaSuccess = False
        while not captchaSuccess:
            captcha = ""
            if self.autoCaptcha :
                captchaIMG = self.driver.find_elements(By.XPATH, '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[3]/div[2]/div/img')[0].screenshot_as_png
                try : 
                    captcha = self.solver.solve_captcha(captchaIMG)
                    # print(captcha)
                except Exception as e :
                    print(e)
                    continue
            else :
                captcha = input("Captcha: ") 

            captchaBox = self.driver.find_elements(By.XPATH, '//*[@id="txt_ALPHA_NUMERIC"]')[0]
            captchaBox.send_keys(captcha.upper())

            submit = self.driver.find_elements(By.XPATH, '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[4]/div/button')[0]
            submit.click()

            spinner = self.driver.find_elements(By.XPATH, '/html/body/form/div[3]')[0]

            time.sleep(0.1)
            while spinner.is_displayed() :
                time.sleep(0.1)
            time.sleep(0.2)
            
            try:
                err = self.driver.find_elements(By.XPATH, '/html/body/form/div[1]/div[3]/div/div[2]/div/div/div[2]/div[1]/div/div/div/ul/li/span')[0].text
                print(err)
                captchaSuccess = err != "Verification Code Mismatch"
            except Exception as e:
                # print(e)
                captchaSuccess = True

            if not captchaSuccess:
                print("retrying captcha")
        

        data = {}
        for x in self.xPaths :
            key     = x[0]
            try :
                value   = self.driver.find_elements(By.XPATH, x[1])[0].text
                value = value.replace(' click here to see CERSAI details', '')
            except Exception as e:
                value = x[2]
            data[key] = value

        return data

    def close( self ):
        self.driver.close()